package main;

public class Judge {
  private boolean judge;  // true:重複あり, false:重複なし
  private int[] checkNumbers;
  private Number target;

  // constructor
  public Judge() {
    this.target = new Number();
  }
  
  public Judge(Number target) {
    this();
    target = new Number();
  }
  
  // 判定
  public boolean judge(int[][] collection) {
    collection = new int[9][9];
    System.out.println("判定メソッド");
    this.judge = false;
    for (int i=0; i<collection.length; i++) {
      System.out.println(i+1+"行目");
      this.judge = judgeLine(collection[i]);
      System.out.println(collection[i][0]);
      if (!judge) {return judge;}
      System.out.println("judge"+judge);
    }
    
    return this.judge;
  }

  // ラインの判定(縦横分ける?)
  public boolean judgeLine(int[] checkNumbers) {
    for (int i=0; i<checkNumbers.length; i++) {
      for (int j=i+1; j<checkNumbers.length; j++) {
        if (checkNumbers[i] == checkNumbers[j]) {
          System.out.println(checkNumbers[i] +"i : j"+ checkNumbers[j]);
          return true;
        }
      }
    }
    return false;
  }

  // エリアの判定
  
  /* Number.javaで作ったメソッド
  // 判定用メソッド
  // true:重複あり, false:重複なし
  public boolean judge() {
    System.out.println("判定メソッド");
    this.judge = false;
    
    // 列での判定row
    System.out.println("列判定開始");
    for (int i=0; i<size; i++) {
      //this.judge = judgeRow(numbers[i]);
      for (int j=0; j<size; j++) {
        for (int k=j+1; k<size; k++) {
          if (numbers[i][j] == numbers[i][k]) {
            // 列に同じ数字があったらtrue（重複有）を返す
            System.out.println("重複有");
            this.judge = true;
            break;
          }
        }
      }
    }
    if (this.judge) {return this.judge;}
    System.out.println("列判定終了");
    
    // 行での判定column
    System.out.println("行判定開始");
    for (int i=0; i<size; i++) {
      for (int j=0; j<size; j++) {
        for (int k=j+1; k<size; k++) {
          if (numbers[j][i] == numbers[k][i]) {
            // 行に同じ数字があったらtrue（重複有）を返す
            System.out.println("重複有");
            this.judge = true;
            break;
          }
        }
      }
    }
    if (this.judge) {return this.judge;}
    System.out.println("行判定終了");
    
    // エリアでの判定area
    System.out.println("エリア判定開始");
    int[] checkList = new int[size]; //checkList[?] = numbers[?][?]
    // i,jは各エリアの左上をさすようにする. x,yを動かしてnumberから数値を取る
    for (int i=0; i<size; i+=3) {
      for (int j=0; j<size; j+=3) {
        for (int k=0; k<size; k++) {
          // 3×3の正方形で左から右、上から下へと動かし、その値を1次元の配列に置く
          int x = k/3 + i;
          int y = k%3 + j;
          checkList[k] = numbers[x][y];
          System.out.println(x+" "+y+" "+checkList[k]);
        }
        for (int k=0; k<size; k++) {
          for (int l=k+1; l<size; l++) {
            if (checkList[k] == checkList[l]) {
              // エリアに同じ数字があったらtrue（重複有）を返す
              System.out.println("重複有");
              this.judge = true;
              break;
            }
          }
        }
      }
    }
    if (this.judge) {return this.judge;}
    System.out.println("エリア判定終了");
    
    return this.judge;
  }
  */
}
