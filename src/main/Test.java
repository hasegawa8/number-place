package main;
/*
 * 数独を作る上での第一段階
 * 数字が埋まっている状態を想定して、それが正解かどうかの処理を作る。
 * 
 */
public class Test {
  public static void main(String[] args) {
    int[][] numberTest = {  // 数独のフィールドが全て埋まっている状態
        {1,2,3,4,5,6,7,8,9},
        {4,5,6,7,8,9,1,2,3},
        {7,8,9,1,2,3,4,5,6},
        {2,3,1,5,6,4,8,9,7},
        {5,6,4,8,9,7,2,3,1},
        {8,9,7,2,3,1,5,6,4},
        {3,1,2,6,4,5,9,7,8},
        {6,4,5,9,7,8,3,1,2},
        {9,7,8,3,1,2,6,4,5},        
    };
    
    Number numberCollection;
    numberCollection = new Number();
    numberCollection.setNumbers(numberTest);
    numberCollection.showNumbers();
    
    numberCollection.judge();
    Judge kakunin = new Judge(numberCollection);
    //kakunin.judge(numberTest);
  }
}
