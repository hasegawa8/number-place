package main;

public class Number {
  private int[][] numbers;
  private boolean judge;
  private final int size = 9;

  // constructor
  public Number() {
    this.numbers = new int[size][size];
    this.resetNumbers();
  }

  public Number(int[][] numbers) {
    this();
    this.setNumbers(numbers);
  }

  // 与えられた配列をセット
  public void setNumbers(int[][] numbers) {
    for (int i=0; i<size; i++) {
      for (int j=0; j<size; j++) {
        this.numbers[i][j] = numbers[i][j];
      }
    }
  }
  
  // 全ての数値を0にする
  public void resetNumbers() {
    for (int i=0; i<size; i++) {
      for (int j=0; j<size; j++) {
        this.numbers[i][j] = 0;
      }
    }
  }
  
  // 今のフィールドを表示
  public void showNumbers() {
    String lf=System.getProperty("line.separator");
    for (int i=0; i<size; i++) {
      for (int j=0; j<size; j++) {
        System.out.print(numbers[i][j] + " ");
      }
      System.out.print(lf);
    }
  }
  
  //
  public int getSize() {
    return this.size;
  }
  
  // 判定用メソッド
  // true:重複あり, false:重複なし
  public boolean judge() {
    System.out.println("判定メソッド");
    this.judge = false;
    
    // 列での判定row
    System.out.println("列判定開始");
    for (int i=0; i<size; i++) {
      //this.judge = judgeRow(numbers[i]);
      for (int j=0; j<size; j++) {
        for (int k=j+1; k<size; k++) {
          if (numbers[i][j] == numbers[i][k]) {
            // 列に同じ数字があったらtrue（重複有）を返す
            System.out.println("重複有");
            this.judge = true;
            break;
          }
        }
      }
    }
    if (this.judge) {return this.judge;}
    System.out.println("列判定終了");
    
    // 行での判定column
    System.out.println("行判定開始");
    for (int i=0; i<size; i++) {
      for (int j=0; j<size; j++) {
        for (int k=j+1; k<size; k++) {
          if (numbers[j][i] == numbers[k][i]) {
            // 行に同じ数字があったらtrue（重複有）を返す
            System.out.println("重複有");
            this.judge = true;
            break;
          }
        }
      }
    }
    if (this.judge) {return this.judge;}
    System.out.println("行判定終了");
    
    // エリアでの判定area
    System.out.println("エリア判定開始");
    int[] checkList = new int[size]; //checkList[?] = numbers[?][?]
    // i,jは各エリアの左上をさすようにする. x,yを動かしてnumberから数値を取る
    for (int i=0; i<size; i+=3) {
      for (int j=0; j<size; j+=3) {
        for (int k=0; k<size; k++) {
          // 3×3の正方形で左から右、上から下へと動かし、その値を1次元の配列に置く
          int x = k/3 + i;
          int y = k%3 + j;
          checkList[k] = numbers[x][y];
          System.out.println(x+" "+y+" "+checkList[k]);
        }
        for (int k=0; k<size; k++) {
          for (int l=k+1; l<size; l++) {
            if (checkList[k] == checkList[l]) {
              // エリアに同じ数字があったらtrue（重複有）を返す
              System.out.println("重複有");
              this.judge = true;
              break;
            }
          }
        }
      }
    }
    if (this.judge) {return this.judge;}
    System.out.println("エリア判定終了");
    
    return this.judge;
  }
}
