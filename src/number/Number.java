package number;

/*
 * int型一次元配列 numbers を持つ
 * int型 arraySize を持つ, 配列のサイズを保存する 
 */
public class Number {
  private int[] numbers;
  private int arraySize;
  
  // constructor default
  public Number() {
    arraySize = 9;
    this.numbers = new int[arraySize];
  }
  // constructor int[]
  public Number(int[] numbers) {
    this();
    setNumbers(numbers);
  }
  
  // setter
  public void setNumbers(int[] numbers) {
    for (int i=0; i<arraySize; i++) {
      this.numbers[i] = numbers[i];
    }
  }
  
  // getter
  public int[] getNumbers() {return numbers;}
  public int getArraySize() {return arraySize;}
  
  // numbers　の中身を表示
  public void showNumbers() {
    System.out.print("{");
    for (int i=0; i<arraySize; i++) {
      System.out.print(" "+numbers[i]);
    }
    System.out.print("}");
  }
  
  @Override
  public String toString() {
    showNumbers();
    return "";
  }
  
  // Number クラスの動作確認用
  public static void main(String[] args) {
    int[] test01 = {1,2,3,4,5,6,7,8,9};
    Number testNumber01 = new Number();
    testNumber01.setNumbers(test01);
    System.out.println(testNumber01);
    System.out.println("numberArrayLength="+testNumber01.getArraySize());
  }
}
