package judgement;

import number.Number;

/*
 * 1行の数字、1ブロックの数字に
 * 同じ数字があるかどうかの判定
 * 
 * Judgeクラスの judgeメソッド を実装するためのクラス
 */
public class JudgeOneToNine extends Judge {
  
  // constructor default
  public JudgeOneToNine() {
    super();
  }
  // constructor Number
  public JudgeOneToNine(Number numberCollect) {
    super(numberCollect);
  }
  // constructor int[]
  public JudgeOneToNine(int[] numbers) {
    super(numbers);
  }
  
  // 2つの変数が等しかったらtrue, 等しくなかったらfalse
  public boolean judge(int number, int target) {
    if (number==target) setJudge(true);
    else setJudge(false);
    return isJudge();
  }
  
  // numberCollectの中に重複があるかどうかの判定
  @Override
  public boolean judge() {
    for (int i=0; i<numberCollect.getArraySize(); i++) {
      for (int j=i+1; j<numberCollect.getArraySize(); j++) {
        if (judge(numberCollect.getNumbers()[i], numberCollect.getNumbers()[j]))
          return isJudge();
      }
    }
    setJudge(false);
    return isJudge();
  }

  // このクラスの動作確認用
  public static void main(String[] args) {
    int[] line01 = {1,2,3,4,5,6,7,8,9};
    Number testNumber01 = new Number(line01);
    System.out.println(testNumber01);
    JudgeOneToNine test01 = new JudgeOneToNine();
    test01.setNumber(testNumber01);
    System.out.println(test01);

    
    int[] line02 = {1,2,3,4,5,6,7,8,2};
    Number testNumber02 = new Number(line02);
    System.out.println(testNumber02);
    JudgeOneToNine test02 = new JudgeOneToNine(line02);
    System.out.println(test02);
  }
}
