package judgement;

import number.Number;

/* 
 * ある数字群に重複があるかどうかを判定するための基礎情報
 * Number型の numberCollect を持つ
 * boolean型の judge を持つ, 
 *   ------numberCollect が重複あり(・0あり):true, 重複なし:false
 */
public abstract class Judge {
  protected Number numberCollect;
  protected boolean judge;
  
  // constructor default
  public Judge() {
    this.numberCollect = new Number();
    this.judge = true;
  }
  //constructor Number
  public Judge(Number numberCollect) {
    this();
    this.numberCollect = numberCollect;
    judge();
  }
  //constructor int[]
  public Judge(int[] numbers) {
    this();
    this.numberCollect.setNumbers(numbers);
    judge();
  }

  // setter
  public void setNumber(Number setCollect) {
    //this.numberCollect = setCollect;
    this.numberCollect.setNumbers(setCollect.getNumbers());
    judge();
  }
  public void setNumber(int[] numbers) {
    this.numberCollect.setNumbers(numbers);
    judge();
  }
  protected void setJudge(boolean judge) {
    this.judge = judge;
  }
  
  // getter
  public boolean isJudge() {
    return judge;
  }
  
  // judgeメソッドの抽象メソッド
  public abstract boolean judge();
  
  // numberCollect の中身を表示
  public void showNumberCollect() {
    numberCollect.showNumbers();
  }
  
  @Override
  public String toString() {
    showNumberCollect();
    System.out.print(isJudge());
    return "";
  }

}
