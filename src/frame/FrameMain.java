package frame;

import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class FrameMain extends JFrame{

  public FrameMain() {
    super("testFrame");
    this.setSize(550, 750);
    this.setDefaultCloseOperation(EXIT_ON_CLOSE);

    Container contentPane = getContentPane();

    // Outline の表示
    Outline canvas = new Outline();
    contentPane.add(canvas);

    // 1～9のボタンの表示
    JPanel button = new JPanel();
    button.setLayout(null);
    int size = 50;
    JButton b1 = new JButton("１");
    b1.setBounds(200, 450, size, size);
    JButton b2 = new JButton("２");
    b2.setBounds(250, 450, size, size);
    JButton b3 = new JButton("３");
    b3.setBounds(300, 450, size, size);
    JButton b4 = new JButton("４");
    b4.setBounds(200, 500, size, size);
    JButton b5 = new JButton("５");
    b5.setBounds(250, 500, size, size);
    JButton b6 = new JButton("６");
    b6.setBounds(300, 500, size, size);
    JButton b7 = new JButton("７");
    b7.setBounds(200, 550, size, size);
    JButton b8 = new JButton("８");
    b8.setBounds(250, 550, size, size);
    JButton b9 = new JButton("９");
    b9.setBounds(300, 550, size, size);
    button.add(b1);
    button.add(b2);
    button.add(b3);
    button.add(b4);
    button.add(b5);
    button.add(b6);
    button.add(b7);
    button.add(b8);
    button.add(b9);

    contentPane.add(button);

    this.setVisible(true);
  }

  public static void main(String[] args) {
    new FrameMain();
  }
}
