package frame;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;

public class Outline extends JComponent{
  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    // 数独のフィールドの線
    g.setColor(Color.BLUE);
    int baseX = 20;
    int baseY = 20;
    int size = 50;
    for (int i=0; i<10; i++) {
      g.drawLine(baseX, baseY+size*i, baseX+size*9, baseY+size*i);
      g.drawLine(baseX+size*i, baseY, baseX+size*i, baseY+size*9);
    }

    // 1～9ボタンの置く位置
    g.setColor(Color.RED);
    baseX = 320;
    baseY = 500;
    size = 50;
    for (int i=0; i<4; i++) {
      g.drawLine(baseX, baseY+size*i, baseX+size*3, baseY+size*i);
      g.drawLine(baseX+size*i, baseY, baseX+size*i, baseY+size*3);
    }

 // 1～9のボタンの表示
    JPanel button = new JPanel();
    button.setLayout(null);
    size = 50;
    JButton b1 = new JButton("１");
    b1.setBounds(200, 450, size, size);
    JButton b2 = new JButton("２");
    b2.setBounds(250, 450, size, size);
    JButton b3 = new JButton("３");
    b3.setBounds(300, 450, size, size);
    JButton b4 = new JButton("４");
    b4.setBounds(200, 500, size, size);
    JButton b5 = new JButton("５");
    b5.setBounds(250, 500, size, size);
    JButton b6 = new JButton("６");
    b6.setBounds(300, 500, size, size);
    JButton b7 = new JButton("７");
    b7.setBounds(200, 550, size, size);
    JButton b8 = new JButton("８");
    b8.setBounds(250, 550, size, size);
    JButton b9 = new JButton("９");
    b9.setBounds(300, 550, size, size);
    button.add(b1);
    button.add(b2);
    button.add(b3);
    button.add(b4);
    button.add(b5);
    button.add(b6);
    button.add(b7);
    button.add(b8);
    button.add(b9);
  }
}
