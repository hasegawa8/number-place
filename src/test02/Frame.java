package test02;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import judgement.Judge;
import judgement.JudgeOneToNine;
import number.Number;


/*---改善点----
 * textField 値を取得する→文字が入っているときの処理
 * 多重ループの改善
 * textFieldの幅の改善
 * 判定の処理をクラス化→列単位（9数）での判定をできるクラスを作った
 * numberのクラス化→クラスを作ったから使えるようにする
 * 
 */

public class Frame extends JFrame implements ActionListener {
  
  // 画面に表示させるときに使うパネル
  private JPanel panelField;   // 9*9のフィールドを表示するためのパネル
  private JPanel panelButton;  // ボタンを表示するためのパネル
  
  // テキストフィールド
  private JTextField[][] numberArea81;  // 9*9の数字を入れるフィールド
  
  // ボタン用の変数
  private JButton btnSet;    // 処理するために配列に登録するボタン
  private JButton btnJudge;  // 重複がないかどうかの判定をするボタン
  
  // 処理用の変数
  private int[][] number81;           // 9*9の全ての数字
  private Number numberTemp;          // Number型の仮変数
  private int[] numberLine;           // 1*9,9*1の列の数字
  private int size;                   // 配列のサイズ用変数
  private boolean fullset;            // フィールドにすべてあるかの確認用変数
  private JudgeOneToNine judgeNumber; // listで管理する前に置く変数
  private ArrayList<JudgeOneToNine> judgeList = 
                 new ArrayList<JudgeOneToNine>();  // judgeNumber のlist
  
  // フォント用の変数
  private Font fontField;   // フィールド用のフォント　
  private Font fontButton;  // ボタン用のフォント
  
  
  public Frame() {
    super("number place");
    this.setSize(550, 600);
    this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    Container contentPane = getContentPane();
    
    // パネルの初期化
    panelField = new JPanel();
    panelButton = new JPanel();
    
    // フォントの初期化
    fontField = new Font("Serif", Font.BOLD, 26);
    fontButton = new Font("Serif", Font.BOLD, 20);
    
    // テキストフィールドの初期化
    size = 9;
    numberArea81 = new JTextField[size][size];
    for (int i=0; i<size; i++){
      for (int j=0; j<size; j++){
        numberArea81[i][j] = new JTextField("", 1);
      }
    }
    
    // 処理用配列の初期化
    number81 = new int[size][size];
    for (int i=0; i<size; i++){
      for (int j=0; j<size; j++){
        number81[i][j] = 0;
      }
    }
    numberLine = new int[size];
    for (int i=0; i<size; i++) {
      numberLine[i] = 0; 
    }
    
    
    // 入力するエリアを表示する
    GridLayout panelLayout = new GridLayout(size, size); // 9*9の格子状
    panelLayout.setHgap(5);                              // 水平方向の隙間が5
    panelLayout.setVgap(5);                              // 垂直方向の隙間が5
    panelField.setLayout(panelLayout);
    
    for (int i=0; i<size; i++){
      for (int j=0; j<size; j++){
        numberArea81[i][j].setHorizontalAlignment(JTextField.CENTER);
        numberArea81[i][j].setFont(fontField);
        panelField.add(numberArea81[i][j]);
      }
    }
    contentPane.add(panelField, BorderLayout.CENTER);
    
    //　ボタンの設定・表示
    btnSet = new JButton("set number");
    btnSet.setFont(fontButton);
    btnSet.addActionListener(this);
    btnJudge = new JButton("judge number");
    btnJudge.setFont(fontButton);
    btnJudge.addActionListener(this);
    
    panelButton.add(btnSet);
    panelButton.add(btnJudge);
    contentPane.add(panelButton, BorderLayout.SOUTH);
    
    this.setVisible(true);
  }
  
  
  // ボタンを押したときの動作の設定
  @Override
  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == btnSet){    // set number ボタンを押したとき
      System.out.println("number set");
      // フィールドにある文字を読み取って
      // number81に数値を入れる処理
      for (int i=0; i<size; i++) {
        for (int j=0; j<size; j++) {
          String text = numberArea81[i][j].getText();
          // 数値以外の値が入っているときの処理が抜けている
          if (text.equals("")) {  // textFieldが空白のときは0を代入
            number81[i][j] = 0;
          //} else if () {  数字以外(文字,記号)が入っているとき
            
          } else {                // それ以外のときの処理
            int num = Integer.parseUnsignedInt(text);
            if (num>=1 && num<=9) number81[i][j] = num; // 数値が1～9のときそのまま代入
            else number81[i][j] = 0;                    // それ以外の数値は0を代入
          }
          System.out.print(" " + number81[i][j]); // check
        }
        System.out.println(); // check
      }
      
      // Judgeクラスに保存、9個の数字とそれに伴う重複判定をまとめて保存する
      // Judgeクラスに保存すると同時にその数字群の重複判定も行う
      judgeList.clear();
      setRow();
      setColumn();
      setArea();
      
    } else if (e.getSource() == btnJudge) {    // judge number ボタンを押したとき
      //　number81　の中に0が入っているときの処理
      // fullset
      // true:0がある, false:0がない
      fullset = false;
      for (int i=0; i<size; i++) {
        for (int j=0; j<size; j++) {
          if (number81[i][j]==0) fullset = true;
        } 
      }
      
      // 全てのtextFieldに数字が正しくあるとき
      if (!fullset){
        showMiss();
      } else {
        System.out.println("正しく入力してください。"); //check
      }
    }
  }
  
  // setJudgeOneToNine
  public void setNineNumbers(int[] numberLine) {
    numberTemp = new Number();
    numberTemp.setNumbers(numberLine);
    judgeNumber = new JudgeOneToNine(numberTemp);
    judgeList.add(judgeNumber);
  }
  
  // setRow
  public void setRow() {
    for (int i=0; i<size; i++) {
      for (int j=0; j<size; j++) {
        numberLine[j] = number81[i][j];
      }
      setNineNumbers(numberLine);
    }
  }
  
  // setColumn
  public void setColumn() {
    for (int i=0; i<size; i++) {
      for (int j=0; j<size; j++) {
        numberLine[j] = number81[j][i];
      }
      setNineNumbers(numberLine);
    }
  }
  
  // setArea
  public void setArea() {
    for (int i=0; i<size; i+=3) {
      for (int j=0; j<size; j+=3) {
        for (int k=0; k<size; k++) {
          // 3×3の正方形で左から右、上から下へと動かし、
          // その値をJudge型で管理する
          int x = k/3 + i;
          int y = k%3 + j;
          numberLine[k] = number81[x][y];
        }
        setNineNumbers(numberLine);
      }
    }
  }

  // 行, 列, エリアのどこに重複があるかの照合
  public void showMiss() {
    for (Judge list : judgeList) {
      if (list.isJudge()) {
        System.out.println(list);
        // 重複がある行,列,エリアが何番目かを表示
      }
    }
  }
  
  /*
  // 列の判定
  public void judgeRow() {
    for (int i=0; i<size; i++) {
      rowBreak: for (int j=0; j<size; j++) {
        for (int k=j+1; k<size; k++) {
          if (number81[i][j] == number81[i][k]) {
            // 列に同じ数字があったらtrue（重複有）を返す
            System.out.println(i+1+"列目");//check
            System.out.println("重複有");//check
            this.fullset = true;
            break rowBreak;
          }
        }
      }
    }
  }
  
  // 行の判定
  public void judgeColumn() {
    for (int i=0; i<size; i++) {
      columnBreak: for (int j=0; j<size; j++) {
        for (int k=j+1; k<size; k++) {
          if (number81[j][i] == number81[k][i]) {
            // 行に同じ数字があったらtrue（重複有）を返す
            System.out.println(i+1+"行目");//check
            System.out.println("重複有");//check
            this.fullset = true;
            break columnBreak;
          }
        }
      }
    }
  }
  
  // 3*3の判定
  public void judgeArea() {
    int count=1;
    // i,jは各エリアの左上をさすようにする
    // x,yを動かしてnumberから数値を取る
    for (int i=0; i<size; i+=3) {
      for (int j=0; j<size; j+=3) {
        for (int k=0; k<size; k++) {
          // 3×3の正方形で左から右、上から下へと動かし、その値を1次元の配列に置く
          int x = k/3 + i;
          int y = k%3 + j;
          numberLine[k] = number81[x][y];
        }
        areaBreak: for (int k=0; k<size; k++) {
          for (int l=k+1; l<size; l++) {
            if (numberLine[k] == numberLine[l]) {
              // エリアに同じ数字があったらtrue（重複有）を返す
              System.out.println(count+++"エリア");//check
              System.out.println("重複有");//check
              this.fullset = true;
              break areaBreak;
            }
          }
        }
      }
    }
  }
  */
  
  // Frameクラスの動作確認用main
  public static void main(String[] args) {
    Frame testFrame = new Frame();
    ArrayList<Judge> listTest = new ArrayList<Judge>();
    int[][] numberSudoku = {  // 数独のフィールドが全て埋まっている状態
        {1,2,3,4,5,6,7,8,9},
        {4,5,6,7,8,9,1,2,3},
        {7,8,9,1,2,3,4,5,6},
        {2,3,1,5,6,4,8,9,7},
        {5,6,4,8,9,7,2,3,1},
        {8,9,7,2,3,1,5,6,4},
        {3,1,2,6,4,5,9,7,8},
        {6,4,5,9,7,8,3,1,2},
        {9,7,8,3,1,2,6,4,5},        
    };
    testFrame.number81 = numberSudoku;
    ActionEvent e=new ActionEvent(testFrame.btnSet, 0, null);
    testFrame.actionPerformed(e);
    testFrame.setRow();
    testFrame.setColumn();
    testFrame.setArea();
    testFrame.showMiss();
    
    
    /*
    ArrayList<Judge> listTest = new ArrayList<Judge>();
    int[][] numberSudoku = {  // 数独のフィールドが全て埋まっている状態
        {1,2,3,4,5,6,7,8,9},
        {4,5,6,7,8,9,1,2,3},
        {7,8,9,1,2,3,4,5,6},
        {2,3,1,5,6,4,8,9,7},
        {5,6,4,8,9,7,2,3,1},
        {8,9,7,2,3,1,5,6,4},
        {3,1,2,6,4,5,9,7,8},
        {6,4,5,9,7,8,3,1,2},
        {9,7,8,3,1,2,6,4,5},        
    };
    int[] numberLine = {2,3,4,5,6,7,8,8,2};
    Number testNumber = new Number(numberLine);
    for (int i=0; i<numberSudoku.length; i++) {
      JudgeOneToNine testRow = new JudgeOneToNine(numberSudoku[i]);
      listTest.add(testRow);
    }
    JudgeOneToNine test = new JudgeOneToNine();
    test.setNumber(testNumber);
    listTest.add(test);
    
    for (Judge j : listTest) {
      System.out.println(j);
    }
    */
  }// main end
}
